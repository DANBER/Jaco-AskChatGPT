FROM docker.io/ubuntu:22.04

ARG DEBIAN_FRONTEND=noninteractive
ENV LANG C.UTF-8
ENV LC_ALL C.UTF-8
WORKDIR /

RUN apt-get update && apt-get upgrade -y
RUN apt-get update && apt-get install -y --no-install-recommends nano wget curl git
RUN apt-get update && apt-get install -y --no-install-recommends python3-pip

# Update pip
RUN pip3 install --upgrade pip
RUN pip3 install --no-cache-dir --upgrade setuptools

# Install jacolib
RUN mkdir /Jaco-Master/
RUN cd /Jaco-Master/; git clone https://gitlab.com/Jaco-Assistant/jacolib.git
RUN pip3 install --no-cache-dir --upgrade --user -e /Jaco-Master/jacolib

# Virtual display
RUN apt-get update && apt-get install -y --no-install-recommends xvfb

# Install chrome-browser
RUN wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
RUN apt-get update && apt-get install -y --no-install-recommends ./google-chrome-stable_current_amd64.deb
RUN pip3 install --no-cache-dir --upgrade undetected_chromedriver

# Install inofficial ChatGPT-API
RUN pip3 install --no-cache-dir --upgrade revChatGPT

# Clear cache to save space, only has an effect if image is squashed
RUN apt-get autoremove -y \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/*
