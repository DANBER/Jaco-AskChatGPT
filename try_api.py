import os

from revChatGPT.V1 import Chatbot

from jacolib import assistant

# ==================================================================================================

filepath = os.path.dirname(os.path.realpath(__file__)) + "/"
assist: assistant.Assistant

# ==================================================================================================


def main():
    global assist, url

    assist = assistant.Assistant(repo_path=filepath)
    auth_data = dict(assist.get_config()["user"])
    auth_data = {k: v for k, v in auth_data.items() if v != "" and v is not False}
    print("Authdata:", auth_data)

    # Use conversation_id and parent_id to start a custom conversation
    chatbot = Chatbot(config=auth_data, conversation_id=None, parent_id=None)

    # You can specify custom conversation and parent ids. Otherwise it uses the saved conversation.
    prompt = "What is the meaning of life? Answer in one sentence."
    for data in chatbot.ask(prompt):
        response = data["message"]

    print("")
    print("Promt:", prompt)
    print("Response:", response)


# ==================================================================================================

if __name__ == "__main__":
    main()
